/**
 * 
 * @param message: ouput
 *  
 */
function printLog(message:any) {
  console.log(message);
}
let message = 'Hello World';
//printLog(message);

/**
 * 
 * @param max max number of output
 */
function doSomething(max:number | any) {
  for(let i = 0; i <= max; i++ )
    log(i);
    //log('finished at : ' + i);
}
//doSomething(5);

/**
 * ojbect example
 */
let a: number[] = [ 1, 2, 3];
//console.log (typeof(a));

/**
 * enum example
 */
enum Color { Red = 0, Orange = 1, Yellow = 2}
let colorBackground = Color.Yellow;
//console.log(colorBackground)  //output 2

/**
 * check last output char
 * endsWith method only support ec6 or least version
 * or add interface
 * interface String {    
     endsWith(searchString: string, endPosition?: number): boolean;
    };
 */
let word = 'abc';
let endwith = (word as string).endsWith('c',3);
let endwithc = (<string>word).endsWith('c');
//console.log(endwith);
//console.log(endwithc);

/**
 * function使用箭頭符號
 * 目的更簡潔 (減少行數, 不需要括號)
 */
let log = function(message: any) {
  console.log(message);
}
let doLog = (message:any) => console.log(message);
//console.log(log('test'));
//console.log(doLog('test'));

/**
 * use interface
 */
let drawPoint = (a:number, b:number, c:number) => {
  // it is bad 
}
let doDrawPoint = (point: {x:number, y:number}) => {
  // middle
  return point.x+point.y;
}
doDrawPoint({
  x: 1,
  y: 2
})
//best
interface IPoint {
    x: number,
        y: number
    
    }
// let doDrawPointBest = (point: IPoint) => {
//     //..
//     point.draw
// }

// class Pointclass {
//     x: number;
//     y: number;
//     draw() {

//     }
// }

/**
 * Number
 * 
 */

let a1 = 10.5;
console.log(typeof(a));


function cz() {
  return 10;
}
console.log(cz);

  